__author__ = 'Igor Tkalenko'

import os
import thread
import platform

import wx
import wx.html
import wx.html2

from Cura.gui import threadingDialog as td
from Cura.util import profile
from Cura.util import thingiverse as ti_web
from Cura.util import meshLoader

__all__ = ['load_collections_logic', ]

# dialog constants:
LO_ST_LOGGED_IN = 1001
LO_ST_CLOSED = wx.ID_CANCEL
# all collections constsnt for select collections dropdown list
SCD_ALL_COLLECTIONS = "-1"

# dialog messages constants
DOWNLOADING_ERROR_QUESTION = ""
DOWNLOADING_EXISTS_QUESTION = ""
def loadDefaultMessages():
    global DOWNLOADING_ERROR_QUESTION, DOWNLOADING_EXISTS_QUESTION
    DOWNLOADING_ERROR_QUESTION = _("Do you want to skip it and continue downloading?")
    DOWNLOADING_EXISTS_QUESTION = _("Do you want to replace existing file?\nIf you click Skip then downloading will be continue without this file.")

# Gui part of Thingiverse logic
class HtmlMessageDialog(wx.Dialog):
    def __init__(self, parent=None, message="", title="", style=wx.ICON_WARNING):
        wx.Dialog.__init__(self, parent, -1, title)

        if style & wx.ICON_WARNING:
            msg_icon = wx.ArtProvider.GetBitmap(wx.ART_WARNING, wx.ART_CMN_DIALOG)
        elif style & wx.ICON_ERROR:
            msg_icon = wx.ArtProvider.GetBitmap(wx.ART_ERROR, wx.ART_CMN_DIALOG)
        elif style & wx.ICON_INFORMATION:
            msg_icon = wx.ArtProvider.GetBitmap(wx.ART_INFORMATION, wx.ART_CMN_DIALOG)
        else:
            msg_icon = None

        color = self.GetBackgroundColour().GetAsString(wx.C2S_HTML_SYNTAX)
        page = '<body bgcolor="%s">%s<body>' % (color, message)

        html = wx.html2.HtmlWindow(self, -1, size=(300, -1), style=wx.html2.HW_NO_SELECTION |
                                                                  wx.html2.HW_SCROLLBAR_NEVER)
        if "gtk2" in wx.PlatformInfo:
            html.SetStandardFonts()
        html.SetPage(page)
        htir = html.GetInternalRepresentation()
        html.SetMinClientSize((htir.GetWidth(), htir.GetHeight()))

        btn_ok = wx.Button(self, wx.ID_OK, _("Ok"))

        sizer = wx.BoxSizer(wx.VERTICAL)
        ctrlSizer = wx.BoxSizer(wx.HORIZONTAL)

        if msg_icon:
            icon_bitmap = wx.StaticBitmap(self, -1, msg_icon, size=msg_icon.GetSize())
            ctrlSizer.Add(icon_bitmap, 0, wx.ALL | wx.ALIGN_RIGHT, 5)
        ctrlSizer.Add(html, 0, wx.ALL | wx.EXPAND, 5)

        sizer.Add(ctrlSizer, 0, wx.ALL | wx.EXPAND, 5)
        sizer.Add(wx.StaticLine(self), 0, wx.LEFT | wx.RIGHT | wx.EXPAND, 5)
        sizer.Add(btn_ok, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        self.SetSizer(sizer)
        sizer.Fit(self)
        self.Center()

        html.Bind(wx.html.EVT_HTML_LINK_CLICKED, self.__link_clicked)

    def __link_clicked(self, e):
        import webbrowser
        href = e.GetLinkInfo().GetHref()
        webbrowser.open(href)


class ReplaceMessageDialog(wx.Dialog):
    def __init__(self, parent=None, message="", title="", style=wx.ICON_WARNING):
        wx.Dialog.__init__(self, parent, -1, title)

        if style & wx.ICON_WARNING:
            msg_icon = wx.ArtProvider.GetBitmap(wx.ART_WARNING, wx.ART_CMN_DIALOG)
        elif style & wx.ICON_ERROR:
            msg_icon = wx.ArtProvider.GetBitmap(wx.ART_ERROR, wx.ART_CMN_DIALOG)
        elif style & wx.ICON_INFORMATION:
            msg_icon = wx.ArtProvider.GetBitmap(wx.ART_INFORMATION, wx.ART_CMN_DIALOG)
        else:
            msg_icon = None

        info = wx.StaticText(self, -1, message)
        btn_ok = wx.Button(self, wx.ID_YES, _("Replace"))
        btn_replace_all = wx.Button(self, wx.ID_YESTOALL, _("Replace All"))
        btn_skip = wx.Button(self, wx.ID_NO, _("Skip"))

        sizer = wx.BoxSizer(wx.VERTICAL)
        ctrlSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer = wx.BoxSizer(wx.HORIZONTAL)

        if msg_icon:
            icon_bitmap = wx.StaticBitmap(self, -1, msg_icon, size=msg_icon.GetSize())
            ctrlSizer.Add(icon_bitmap, 0, wx.ALL | wx.ALIGN_RIGHT, 5)
        ctrlSizer.Add(info, 0, wx.ALL | wx.EXPAND, 5)

        btnSizer.Add(btn_replace_all, 0, wx.ALL | wx.ALIGN_LEFT, 5)
        btnSizer.AddStretchSpacer()
        btnSizer.Add(btn_ok, 0, wx.ALL | wx.ALIGN_RIGHT, 5)
        btnSizer.Add(btn_skip, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        sizer.Add(ctrlSizer, 0, wx.ALL | wx.EXPAND, 5)
        sizer.Add(wx.StaticLine(self), 0, wx.LEFT | wx.RIGHT | wx.EXPAND, 5)
        sizer.Add(btnSizer, 0, wx.ALL | wx.EXPAND, 5)

        self.SetSizer(sizer)
        sizer.Fit(self)
        self.Center()

        # events
        btn_ok.Bind(wx.EVT_BUTTON, self.btn_evt)
        btn_replace_all.Bind(wx.EVT_BUTTON, self.btn_evt)
        btn_skip.Bind(wx.EVT_BUTTON, self.btn_evt)

    def btn_evt(self, e):
        self.EndModal(e.Id)


class LoginDialog(wx.Frame):
    def __init__(self, auth_page, parent, callback):
        super(LoginDialog, self).__init__(parent, title="Thingiverse Login Dialog", style=wx.CAPTION)

        self.__cb = callback
        self.__login_process_runs = False

        panel = wx.Panel(self)

        vbox = wx.BoxSizer(wx.VERTICAL)
        self.brows = wx.html2.WebView.New(panel)
        self.bClose = wx.Button(panel, label="Close")
        self.brows.EnableHistory(False)
        self.brows.LoadURL(auth_page)

        if hasattr(wx.html2, 'EVT_WEBVIEW_LOADED'):
            evt_webview_loaded = wx.html2.EVT_WEBVIEW_LOADED
        elif hasattr(wx.html2, 'EVT_WEB_VIEW_LOADED'):
            evt_webview_loaded = wx.html2.EVT_WEB_VIEW_LOADED
        # if hasattr(wx.html2, 'EVT_WEBVIEW_NAVIGATING'):
            # evt_webview_navigating = wx.html2.EVT_WEBVIEW_NAVIGATING
        # elif hasattr(wx.html2, 'EVT_WEB_VIEW_NAVIGATING'):
            # evt_webview_navigating = wx.html2.EVT_WEB_VIEW_NAVIGATING

        self.brows.Bind(evt_webview_loaded, self.OnPageLoaded)
        #self.Bind(evt_webview_navigating, self.OnPageNavigating, browser)

        vbox.Add(self.brows, proportion=1, flag=wx.ALL | wx.EXPAND, border=5)
        vbox.Add(self.bClose, border=10, flag=wx.ALIGN_RIGHT | wx.ALL)
        panel.SetSizer(vbox)

        self.SetSize((800, 600))
        self.Center()

        # bind events
        self.bClose.Bind(wx.EVT_BUTTON, self.b_close_event)

    def disable_controls(self):
        self.ToggleWindowStyle(wx.CLOSE_BOX)
        self.SetCursor(wx.StockCursor(wx.CURSOR_WAIT))
        self.brows.Disable()
        # self.Disable()

    def enable_controls(self):
        self.ToggleWindowStyle(wx.CLOSE_BOX)
        self.SetCursor(wx.NullCursor)
        self.brows.Enable()
        # self.Enable()

    def b_close_event(self, e):
        self.close_dialog()

    def close_dialog(self):
        if not self.__login_process_runs:
            if platform.system().lower() == 'darwin':
                self.Close()
            else:
                self.Destroy()

    def OnClose(self, e):
        # if not self.__login_process_runs:
        #     e.Veto()
        # else:
        #     # how to apply event???
        print "close window"
        # for window OnClose
        # print "OnClose", e.ShouldPropagate()
        # e.Skip(not self.__login_process_runs)
        # if not self.__login_process_runs:
        #     self.Destroy()

    def OnPageLoaded(self, e):
        url = e.GetURL()
        try:
            code = ti_web.get_value_from_url(url, 'code')
        except:
            code = False
        if code:
            self.disable_controls()
            self.__login_process_runs = True
            thread.start_new_thread(self.lrpLogin, (code, ))

    def lrpLogin(self, code):
        res = ti_web.update_access_token(code)
        wx.CallAfter(self.lrpLoginDone, res)

    def lrpLoginDone(self, res):
        # self.enable_controls()
        self.__login_process_runs = False
        if not isinstance(res, bool):
            if isinstance(res, str) or isinstance(res, unicode):
                dlg = HtmlMessageDialog(self, res, _("Warning"))
                dlg.ShowModal()
                dlg.Destroy()
            self.enable_controls()
            # self.__login_process_runs = False
        else:
            # print 'token returns', res
            wx.FutureCall(10, load_collections_logic_part2, LO_ST_LOGGED_IN, self.GetParent(), self.__cb)
            self.close_dialog()


class SelectCollectionDialog(wx.Dialog):
    def __init__(self, arr, parent=None):
        wx.Dialog.__init__(self, parent, wx.ID_ANY, "Select Collection")
        self.arr = arr

        info_label = wx.StaticText(self, -1, _('Please select collection for downloading from Thingiverse'))

        # Add a panel so it looks the correct on all platforms
        sampleList = []
        selectCollectionCombo = wx.ComboBox(self, size=(240, -1), choices=sampleList, style=wx.CB_READONLY)
        selectCollectionCombo.Append('All last collections', SCD_ALL_COLLECTIONS)
        for c in arr:
            selectCollectionCombo.Append(c.name, c.id)
        selectCollectionCombo.SetSelection(0)
        self.scc = selectCollectionCombo

        ok_btn = wx.Button(self, -1, _('Download'), style=wx.BU_EXACTFIT)
        ok_btn.Bind(wx.EVT_BUTTON, self.onDownload)
        cancel_btn = wx.Button(self, -1, _('Cancel'), style=wx.BU_EXACTFIT)
        cancel_btn.Bind(wx.EVT_BUTTON, self.onClose)

        btn_sizer = wx.BoxSizer(wx.HORIZONTAL)
        btn_sizer.Add(ok_btn, 0, wx.RIGHT | wx.EXPAND, 5)
        btn_sizer.Add(cancel_btn, 0, wx.EXPAND, 0)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(info_label, 0, wx.LEFT | wx.RIGHT | wx.TOP | wx.CENTER, 10)
        sizer.Add(selectCollectionCombo, 0, wx.ALL | wx.EXPAND, 5)
        sizer.Add(wx.StaticLine(self), 0, wx.LEFT | wx.RIGHT | wx.EXPAND, 5)
        sizer.Add(btn_sizer, 1, wx.ALL | wx.ALIGN_RIGHT, 5)

        self.SetSizer(sizer)
        sizer.Fit(self)
        self.Center()

    def onDownload(self, e):
        self.EndModal(wx.ID_OK)

    def get_collection_id(self):
        return self.scc.GetClientData(self.scc.GetSelection())

    def onClose(self, e):
        self.Close()


# class for downloading collections with simply implementation threading
class DownloadCollections(object):
    def __init__(self, collections, parent=None, callback=None):
        object.__init__(self)
        self.__cols = collections
        self.__things = None
        self.__files = None
        self.__par = parent
        self.__cb = callback

        self.__cur_collection = -1
        self.__cur_thing = -1
        self.__cur_file = -1
        self.__cols_in_tree = {}
        self.__ths_in_tree = None
        self.__files_in_tree = None

        self.__d_path = profile.getModelsPath()
        self.__supports_ext = meshLoader.loadSupportedExtensions()
        self.__dlg_name = _("Downloading Collection")
        self.__yes_to_all = False

    def start(self):
        print 'DownloadCollections start'
        self.get_next_collection()

    def done(self):
        print "DownloadCollections done"
        self.remove_folders_if_empty()
        if callable(self.__cb):
            self.__cb(self.__cols_in_tree)

    def get_next_collection(self):
        self.__cur_collection += 1
        if self.__cur_collection < len(self.__cols):
            self.__cur_thing = -1
            col = self.__cols[self.__cur_collection]
            self.run_get_things(col.id, self.get_things_callback)
            # thread.start_new_thread(self.run_get_things, (col.id, self.get_things_callback))
        else:
            self.done()

    def run_get_things(self, col_id, callback):
        ths = None
        exc = None
        try:
            ths = ti_web.get_collection_things(col_id)
        except Exception as e:
            exc = e
        finally:
            if not isinstance(ths, ti_web.Things):
                ths = None
                exc = _("Returned value is not Things json")
        callback(ths, exc)

    def get_things_callback(self, things, exception):
        col = self.__cols[self.__cur_collection]
        col_name = col.name
        if things:
            # self.__dc_path = os.path.join(self.__d_path, col_name)  # moved into get_next_file function
            # self.create_folder_if_needed(self.__dc_path)
            self.__cols_in_tree[col_name] = {}
            self.__ths_in_tree = self.__cols_in_tree[col_name]
            self.__things = things
            return self.get_next_thing()
        if exception:
            dlg = wx.MessageDialog(self.__par,
                "An Error '{}'\noccurred while getting Things for the Collection '{}'\n{}".format(exception,
                col_name, DOWNLOADING_ERROR_QUESTION), _("Question"), wx.YES_NO | wx.ICON_QUESTION | wx.NO_DEFAULT)
            res = dlg.ShowModal()
            dlg.Destroy()
            if res == wx.ID_YES:
                self.get_next_collection()
            else:
                self.done()

    def get_next_thing(self):
        self.__cur_thing += 1
        if self.__cur_thing < len(self.__things):
            self.__cur_file = -1
            thing = self.__things[self.__cur_thing]
            self.run_get_files(thing.id, self.get_files_callback)
            # thread.start_new_thread(self.run_get_files, (thing.id, self.get_files_callback))
        else:
            self.get_next_collection()

    def run_get_files(self, thing_id, callback):
        fls = None
        exc = None
        try:
            fls = ti_web.get_thing_files(thing_id)
        except Exception as e:
            exc = e
        finally:
            if not isinstance(fls, ti_web.ThingFiles):
                fls = None
                exc = _("Returned value is not ThingFiles json")
        callback(fls, exc)

    def get_files_callback(self, files, exception):
        th = self.__things[self.__cur_thing]
        th_name = th.name
        if files:
            # self.__dct_path = os.path.join(self.__dc_path, th_name)  # moved into get_next_file function
            # self.create_folder_if_needed(self.__dct_path)
            ### Add after sucsessfully downloading fisrt file in this thing
            self.__ths_in_tree[th_name] = {}
            self.__files_in_tree = self.__ths_in_tree[th_name]
            ### End
            self.__files = files
            return self.get_next_file()
        if exception:
            dlg = wx.MessageDialog(self.__par, "Exception in Get Files: {}\n {}".format(exception,
               DOWNLOADING_ERROR_QUESTION), _("Question"), wx.YES_NO | wx.ICON_QUESTION | wx.NO_DEFAULT)
            res = dlg.ShowModal()
            dlg.Destroy()
            if res == wx.ID_YES:
                self.get_next_thing()
            else:
                self.done()

    def get_next_file(self):
        self.__cur_file += 1
        if self.__cur_file < len(self.__files):
            tf = self.__files[self.__cur_file]
            tf_name = tf.name
            name, ext = os.path.splitext(tf_name.lower())
            if ext not in self.__supports_ext:
                # print ext, 'not in', self.__supports_ext
                return self.get_next_file()
            th = self.__things[self.__cur_thing]
            th_name = th.name
            col = self.__cols[self.__cur_collection]
            col_name = col.name

            # self.__dc_path = os.path.join(self.__d_path, col_name)  # moved from get_things_callback
            # self.create_folder_if_needed(self.__dc_path)
            # self.__dct_path = os.path.join(self.__dc_path, th_name)  # moved from get_files_callback
            # self.create_folder_if_needed(self.__dct_path)
            # self.__dctf_path = os.path.join(self.__dct_path, tf_name)
            dctf_path = self.generate_full_path(col_name, th_name, tf_name)
            self.__dctf_path = dctf_path
            print dctf_path
            if os.path.exists(dctf_path):
                if not self.__yes_to_all:
                    dlg = ReplaceMessageDialog(self.__par, "File {} is exists. {}".format(tf_name,
                       DOWNLOADING_EXISTS_QUESTION), _("Question"))
                    # dlg = wx.MessageDialog(self.__par, "File {} is exists. {}".format(tf_name,
                    #    DOWNLOADING_EXISTS_QUESTION), _("Question"), wx.YES_NO | wx.ICON_QUESTION | wx.NO_DEFAULT)
                    res = dlg.ShowModal()
                    dlg.Destroy()
                    if res == wx.ID_NO:  # Skip
                        return self.get_next_file()
                    elif res == wx.ID_YES:  # Replace
                        pass
                    elif res == wx.ID_YESTOALL:  # Replace all
                        self.__yes_to_all = True
                    else:  # Cancel
                        return self.done()

            dlg_title = u"%s: %s" % (self.__dlg_name, col_name)
            self.run_download_file(dlg_title, th_name, tf, dctf_path, self.download_file_callback)
        else:
            self.get_next_thing()

    def run_download_file(self, dlg_title, th_name, tf, dctf_path, callback):
        result = True
        # tf_name = tf.name
        tf_url = tf.download_url
        tf_p_url = tf.public_url

        req = ti_web.createGetRequest(tf_url, {'access_token': ti_web.access_token})
        res = ti_web.sendRequest(req)
        if not ti_web.isResponse(res):
            req = ti_web.createGetRequest(tf_p_url, {'access_token': ti_web.access_token})
            res = ti_web.sendRequest(req)
        if ti_web.isResponse(res):
            dlg = td.TDProgressDialog(dlg_title, u'Thing: {}.\nDownloading file'.format(th_name, _('Downloading')))
            thr = td.DownloadThread(res, dctf_path)
            res_d = dlg.ShowModal()
            # print 'downloading progress dialog closed. Result %s' % str(res_d)
            thr_res = thr.getResult()
            del thr
            error = thr_res == td.THR_ERROR
            cancelled = thr_res == td.THR_CANCEL
            # print thr_res, res_d
        else:
            error = res
            cancelled = False
        if error or cancelled:
            result = False
        callback(result, error)

    def download_file_callback(self, result, exception):
        tf = self.__files[self.__cur_file]
        tf_name = tf.name
        if result:
            self.__files_in_tree[tf_name] = self.__dctf_path  # add in table only after downloaded
            # self.__dct_path = os.path.join(self.__dc_path, th_name)
            # self.create_folder_if_needed(self.__dct_path)
            # self.__ths_in_tree[th_name] = {}
            # self.__files_in_tree = self.__ths_in_tree[th_name]
            # self.__files = files
            return self.get_next_file()
        elif not exception:
            self.done()
        else:
            message = "File {} Error occurred. {}".format(tf_name, DOWNLOADING_ERROR_QUESTION)
            print "exception type", type(exception)
            if isinstance(exception, str) or isinstance(exception, unicode):
                message = message + "\n" + exception
            dlg = wx.MessageDialog(self.__par, message, _("Question"), wx.YES_NO | wx.ICON_QUESTION | wx.NO_DEFAULT)
            res = dlg.ShowModal()
            dlg.Destroy()
            if res == wx.ID_YES:
                self.get_next_file()
            else:
                self.done()

    def create_folder_if_needed(self, folder_name):
        if not os.path.exists(folder_name):
            os.mkdir(folder_name)

    def generate_full_path(self, *arr):
        print arr
        f_path = self.__d_path
        for name in arr:
            f_path = os.path.join(f_path, name)  # moved from get_things_callback
            if name != arr[-1]:  # if its folder name
                self.create_folder_if_needed(f_path)
        return f_path

    def remove_folders_if_empty(self):
        pass


# Main function for downloading collections
def load_collections_logic(parent=None, callback=None):
    loadDefaultMessages()
    if ti_web.check_token():
        load_collections_logic_part2(LO_ST_LOGGED_IN, parent, callback)
    else:
        auth_page = ti_web.getThingiverseAuthorisePage()
        win = LoginDialog(auth_page, parent, callback)
        win.Show()

def load_collections_logic_part2(res, parent=None, callback=None):
    print "user", "logged in" if res == LO_ST_LOGGED_IN else "login closed"
    if res == LO_ST_LOGGED_IN:
        try:
            username = ti_web.get_username()
        except Exception as e:
            dlg = wx.MessageDialog(parent, e.message, _("Excetion in get username"), wx.OK | wx.ICON_ERROR)
            res = dlg.ShowModal()
            dlg.Destroy()
            return
        try:
            cols = ti_web.get_collections(username)
        except Exception as e:
            dlg = wx.MessageDialog(parent, e.message, _("Exception in get collection"), wx.OK | wx.ICON_ERROR)
            res = dlg.ShowModal()
            dlg.Destroy()
            return

        # print cols.get_collections_names()
        fc = SelectCollectionDialog(cols, parent)
        res = fc.ShowModal()
        if res == wx.ID_OK:
            c_id = fc.get_collection_id()
            if c_id != SCD_ALL_COLLECTIONS:
                cols = [cols.get_collection_by_id(c_id)]
        fc.Destroy()
        if res == wx.ID_OK:
            dc = DownloadCollections(cols, parent, callback=callback)
            dc.start()

# if __name__ == "__main__":  # only for testing
#     from Cura.util import resources
#
#     profile.loadPreferences(profile.getPreferencePath())
#     resources.setupLocalization(profile.getPreference('language'))  # it's important to set up localization at very beginning to install _
#     loadPreferences()
#     loadDefaultMessages()
#
#     ex = wx.App()
#     load_collections_logic()
#     ex.MainLoop()