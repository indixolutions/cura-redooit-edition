__author__ = 'Igor Tkalenko'

import wx
import wx.html2
from Cura.util import webmodule
from Cura.util import crypto

from Cura.util import profile
from Cura.gui import downloader as dw


class WebViewer(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1)
        sizer = wx.BoxSizer()
        self.browser = wx.html2.WebView.New(self)
        # self.browser.EnableContextMenu(False)
        self.browser.EnableHistory(False)

        self.__current_name = ''
        self.__current_id = ''

        if hasattr(wx.html2, 'EVT_WEBVIEW_LOADED'):
            evt_webview_loaded = wx.html2.EVT_WEBVIEW_LOADED
        elif hasattr(wx.html2, 'EVT_WEB_VIEW_LOADED'):
            evt_webview_loaded = wx.html2.EVT_WEB_VIEW_LOADED

        if hasattr(wx.html2, 'EVT_WEBVIEW_NAVIGATING'):
            evt_webview_navigating = wx.html2.EVT_WEBVIEW_NAVIGATING
        elif hasattr(wx.html2, 'EVT_WEB_VIEW_NAVIGATING'):
            evt_webview_navigating = wx.html2.EVT_WEB_VIEW_NAVIGATING

        self.Bind(evt_webview_loaded, self.OnPageLoaded, self.browser)
        self.Bind(evt_webview_navigating, self.OnPageNavigating, self.browser)
        # self.Bind(wx.html2.EVT_WEBVIEW_NAVIGATED, self.OnPageNavigated, self.browser)

        sizer.Add(self.browser, -1, wx.EXPAND|wx.ALL, 0)
        self.SetSizer(sizer)

        self.__homepageURL = profile.getWebPreference("homepageUrl")
        self.__defaultURL = profile.getWebPreference("defaultUrl")
        webmodule.set_login_callback(self.homePage)
        self.homePage()

    def homePage(self):
        siteToken = webmodule.getSiteToken()
        url = self.__homepageURL + siteToken if webmodule.isConnected() else self.__defaultURL
        self.loadURL(url)

    def loadURL(self, url):
        self.browser.LoadURL(url)

    def OnPageLoaded(self, event):
        url = event.GetURL()
        if webmodule.isPathToSingleModel(url):
            ps = self.browser.GetPageSource()
            ftext = 'data-model-unique-id="'
            pos = ps.find(ftext)
            if pos != -1:
                ps = ps[pos+len(ftext):]
                pos = ps.find('"')
                if pos != -1:
                    model_id = ps[:pos]
                    js = webmodule.getModelByID(model_id)
                    if js:
                        model_name = js.get('name')
                        return

    def OnPageNavigating(self, event):
        url = event.GetURL()
        # disable reloading page
        if self.browser.GetCurrentURL() == event.GetURL():
            #event.Veto()
            return
        # disable going to exception page
        if event.GetURL()[-4:] == ".stl":
            #event.Veto()
            return
        # getting model_id from signal
        if url.find("download") != -1:
            event.Veto()
            model_id = url.split("/")[-1]
            dw.download_model(model_id, dw.DW_ADD_IN_LIB | dw.DW_OPEN, self.GetParent())
