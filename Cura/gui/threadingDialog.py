__author__ = 'Igor Tkalenko'

import wx
import platform
from wx.lib.pubsub import pub
from threading import Thread

import os
import zipfile

# import urllib2
# from unique import url_from, GetUniqueName, download_path

########################################################################
# Constants
########################################################################
THR_NONE = -1
THR_ERROR = 0
THR_CANCEL = 1
THR_OK = 2
THR_WORKING = 3


########################################################################
# threading superclass for TDProgressDialog
########################################################################
class BaseProgessDialogThread(Thread):
    def __init__(self):
        Thread.__init__(self)
        pub.subscribe(self.closeThread, "dialogColse")
        self._status = THR_NONE

    def cancelled(self):
        pass

    def _result(self):
        pass

    def getResult(self):
        res = self._status
        if self._status == THR_OK:
            res = self._result()
            if not res:
                res = THR_OK
        return res

    def __del__(self):
        # print "del Thread"
        if self._status == THR_ERROR or self._status == THR_CANCEL:
            self.cancelled()

    def closeThread(self, msg):
        # print "closeThread"
        self._status = THR_CANCEL

    def send_name(self, name):
        wx.CallAfter(pub.sendMessage, "threadName", name=name)

    def send_value(self, val):
        wx.CallAfter(pub.sendMessage, "threadUpdated", val=val)

    def send_finish(self, message):
        wx.CallAfter(pub.sendMessage, "threadFinished", msg=message)


########################################################################
# thread for downloading file from web
########################################################################
class DownloadThread(BaseProgessDialogThread):
    def __init__(self, responce, downloadPath):
        BaseProgessDialogThread.__init__(self)
        self.__resp = responce
        self.__dlpath = downloadPath
        tmp, self.__dlname = os.path.split(downloadPath)
        self.start()  # start the thread after creation

    def cancelled(self):
        if os.path.exists(self.__dlpath):
            os.remove(self.__dlpath)
            print "file removed"

    def run(self):
        # This is the code executing in the new thread.
        # wx.CallAfter(pub.sendMessage, "threadName", name=self.__dlname)
        self.send_name(self.__dlname)
        webfile = self.__resp
        self._status = THR_WORKING
        size = webfile.info().get('Content-Length')
        if size is not None:
            size = int(size)
            localfile = open(self.__dlpath, 'wb')
            # Download the file
            block_size = 4096
            read = 0
            # while downloading and not cancelled
            while read < size and self._status != THR_CANCEL:
                data = webfile.read(block_size)
                localfile.write(data)
                read += len(data)
                # wx.CallAfter(pub.sendMessage, "threadUpdated", val=float(read)*100.0/float(size))
                self.send_value(float(read)*100.0/size)
            localfile.close()

            if self._status != THR_CANCEL:
                if os.path.exists(self.__dlpath):
                    self._status = THR_OK
            # print 'thread Finished with %s' % str(self._status)
        if self._status == THR_WORKING:
            self._status = THR_ERROR

        # wx.CallAfter(pub.sendMessage, "threadFinished", msg=self._status)
        self.send_finish(self._status)


########################################################################
# thread for extracting files from zip archive
########################################################################
class UnpackingThread(BaseProgessDialogThread):
    def __init__(self, archive, exctractPath):
        BaseProgessDialogThread.__init__(self)
        self.__arc = archive
        self.__extractPath = exctractPath
        self.extracted = []
        self.__exFolders = []
        self.start()  # start the thread after creation

    def _result(self):
        return self.extracted

    def cancelled(self):
        if os.path.exists(self.__extractPath):
            # removing extracted files
            for fp in self.extracted:
                if os.path.exists(fp):
                    os.remove(fp)
            # extend list with all folders
            k = []
            for fpd in self.__exFolders:
                if fpd:
                    ex_path, ex_file = os.path.split(fpd)
                    while ex_file and ex_path:
                        k.append(ex_path)
                        ex_path, ex_file = os.path.split(ex_path)
            self.__exFolders.extend(k)
            # sorting folders for removing lower items first
            self.__exFolders.sort()
            self.__exFolders.reverse()
            # removing folders
            for fpd in self.__exFolders:
                rem_fpd = os.path.join(self.__extractPath, fpd)
                if os.path.exists(rem_fpd):
                    try:
                        os.rmdir(rem_fpd)
                    except Exception, e:
                        print "Folder can't remove Error: %s" % e
            print 'extracted files and folders removed'

    def run(self):
        try:
            zf = zipfile.ZipFile(self.__arc, 'r')
        except:
            self._status = THR_ERROR
            # wx.CallAfter(pub.sendMessage, "threadFinished", msg=self._status)
            self.send_finish(self._status)
            return
        if zf.testzip() is not None:
            self._status = THR_ERROR
            # wx.CallAfter(pub.sendMessage, "threadFinished", msg=self._status)
            self.send_finish(self._status)
            return
        self._status = THR_WORKING
        all_size = sum((file.file_size for file in zf.infolist()))
        curr_size = 0
        block_size = 4096
        for file in zf.infolist():
            fname = file.filename
            fsize = file.file_size
            if fsize == 0:
                continue
            # wx.CallAfter(pub.sendMessage, "threadName", name=fname)
            self.send_name(fname)

            ex_path, ex_file = os.path.split(fname)
            write_folder = os.path.join(self.__extractPath, ex_path)
            if not os.path.exists(write_folder):
                self.__exFolders.append(ex_path)
                os.makedirs(write_folder)

            write_file = os.path.join(self.__extractPath, fname)
            f = zf.open(fname)
            wf = open(write_file, 'wb')
            total_read = 0
            while self._status != THR_CANCEL:
                data = f.read(block_size)
                if not data:
                    break
                total_read += len(data)
                wf.write(data)
                # wx.CallAfter(pub.sendMessage, "threadUpdated", val=float(curr_size + total_read)*100.0/float(all_size))
                self.send_value(float(curr_size + total_read)*100.0/all_size)
            wf.close()
            f.close()
            curr_size += fsize

            self.extracted.append(write_file)
            if self._status == THR_CANCEL:
                break
        zf.close()
        if self._status == THR_WORKING:
            self._status = THR_OK if all_size == curr_size else THR_ERROR
        # wx.CallAfter(pub.sendMessage, "threadFinished", msg=self._status)
        self.send_finish(self._status)


########################################################################
# class for downloading and extracting
########################################################################
class TDProgressDialog(wx.Dialog):
    def __init__(self, title='Progress Dialog', label='Progress'):
        wx.Dialog.__init__(self, None, title=title, style=wx.CAPTION)

        self.progress = wx.Gauge(self, 100)
        self.label = wx.StaticText(self, label=label)
        self.bClose = wx.Button(self, label="Close")
        sz = self.progress.GetSize()
        sz.SetWidth(320)
        self.progress.SetMinSize(sz)

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(sizer)

        sizer.Add(self.label, border=10, flag=wx.EXPAND|wx.ALL)
        sizer.Add(self.progress, border=10, flag=wx.EXPAND|wx.LEFT|wx.RIGHT)
        sizer.Add(self.bClose, border=10, flag=wx.ALIGN_RIGHT|wx.ALL)
        self.Fit()
        self.Centre()

        # bind events
        self.bClose.Bind(wx.EVT_BUTTON, self.OnClose)

        # set values
        self.__question = False
        self.__finished = False
        self.__label = label

        # subscribe pubsub listeners
        pub.subscribe(self.updateProgress, "threadUpdated")
        pub.subscribe(self.closeDialog, "threadFinished")
        pub.subscribe(self.setLabel, "threadName")

    def __del__(self):
        # unsubscribe pubsub listeners
        pub.unsubscribe(self.updateProgress, "threadUpdated")
        pub.unsubscribe(self.closeDialog, "threadFinished")
        pub.unsubscribe(self.setLabel, "threadName")

    def setLabel(self, name):
        self.label.SetLabel(u'{}: {}'.format(self.__label, name))

    def closeDialog(self, msg):
        # print "threadClosed with: %s" % ('Error','Cancel','Done')[msg]
        self.__finished = True
        if not self.__question:
            self.__MyDestroyDialog()

    def __MyDestroyDialog(self):
        # print 'destroy DIALOG'
        if platform.system().lower() == 'darwin':
            wx.Dialog.Close(self)
        else:
            self.Destroy()
        # print 'dialog destoyed'

    def OnClose(self, event):
        dlg = wx.MessageDialog(self, _("Do you want to cancel downloading?"), _("Question"), wx.YES_NO|wx.ICON_QUESTION|wx.NO_DEFAULT)
        self.__question = True
        res = dlg.ShowModal()
        self.__question = False
        # dlg.Destroy()
        if platform.system().lower() == 'darwin':
            dlg.Close()
        else:
            dlg.Destroy()
        if res == wx.ID_YES:
            self.bClose.Disable()
            # print "sendMessage"
            wx.CallAfter(pub.sendMessage, "dialogColse", msg="")
        if self.__finished:
            self.__MyDestroyDialog()
        event.Skip()

    def updateProgress(self, val):
        self.progress.SetValue(val)
