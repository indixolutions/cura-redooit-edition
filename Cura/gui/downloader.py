__author__ = 'Igor Tkalenko'

from Cura.util import webmodule
import webbrowser
import wx

__all__ = ["init_callbacks", "download_model"]

# defines
DW_OPEN = 0b01
DW_ADD_IN_LIB = 0b10


# Callbacks
__model_exists_cb = None
__get_models_cb = None
__add_in_lib_cb = None
__load_files_cb = None

# parent wxWindow for Dialogs
__parent = None

# flags for callbacks
__current_flag = 0

# current model name and id
__current_name = None
__current_id = None


def init_callbacks(me_cb, get_models_cb, add_in_lib_cb, load_models_cb):
    global __model_exists_cb, __get_models_cb, __add_in_lib_cb, __load_files_cb
    __model_exists_cb = me_cb
    __get_models_cb = get_models_cb
    __add_in_lib_cb = add_in_lib_cb
    __load_files_cb = load_models_cb

def download_model(model_id, flags, parent=None):
    global __parent, __current_id, __current_name, __current_flag, __model_exists_cb, __get_models_cb
    __parent = parent
    __current_flag = flags
    if not __model_exists_cb(model_id):
        js = webmodule.getModelByID(model_id)
        __current_id = model_id
        name = js.get('name') if js else False
        if name:
            __current_name = name
        else:
            __current_name = 'model %s' % model_id
        webmodule.downloadModelByID(model_id, parent, __downloading_callback)
    elif __current_flag & DW_OPEN:
        res = __get_models_cb(model_id)
        if __current_flag & DW_ADD_IN_LIB:
            if len(res) == 1:
                __load_files_cb(res)
            else:
                dlg = wx.MessageDialog(parent, _("You have already downloaded this file"), _("Info"), wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
        else:
            __load_files_cb(res)
    else:
        dlg = wx.MessageDialog(parent, _("You have already downloaded this file"), _("Info"), wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()

def __downloading_callback(res):
    global __parent, __current_flag, __current_name, __current_id, __add_in_lib_cb, __load_files_cb
    if isinstance(res, list):
        if __current_flag & DW_ADD_IN_LIB:
            __add_in_lib_cb(__current_name, __current_id, res)

        if __current_flag & DW_OPEN:
            if __current_flag & DW_ADD_IN_LIB:
                if len(res) == 1:
                    __load_files_cb(res)
            else:
                __load_files_cb(res)
    elif isinstance(res, str) or isinstance(res, unicode):
        if res == webmodule.CONNECTION_ERROR or res == webmodule.CONNECTION_ERROR_MSG:
            msg = webmodule.CONNECTION_ERROR_MSG
            title = _("Missing internet connection")
            flag = wx.ICON_ERROR
        else:
            msg = res
            title = _("WebViewer")
            flag = wx.ICON_INFORMATION
        dlg = wx.MessageDialog(__parent, msg, title, wx.OK | flag)
        dlg.ShowModal()
        dlg.Destroy()
    elif isinstance(res, dict):
        if res.get('requirement') == u'purchase required':
            url = res.get('url')
            if url:
                dlg = wx.MessageDialog(__parent, _("Your system browser will now be opened to complete the purchase on our website. Once completed, you can come back here and download the model file"), _("Purchase Model"), wx.OK | wx.CANCEL | wx.ICON_QUESTION | wx.CENTRE)
                if dlg.ShowModal() == wx.ID_OK:
                    fullurl = "".join((url, __current_id))
                    webbrowser.open(fullurl, new=0, autoraise=True)
                dlg.Destroy()