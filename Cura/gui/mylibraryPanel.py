__author__ = 'Igor Tkalenko'

import wx
import wx.html2
from Cura.util import profile
from Cura.util import meshLoader
from Cura.util import crypto
from Cura.util import webmodule
import platform

import xml.etree.ElementTree as ET
import os
import shutil
import re

from xml.dom import minidom
def prettify(elem):
    ### Return a pretty-printed XML string for the Element.
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


class MyLibraryPanel(wx.Panel):
    def __init__(self, parent, backCallback, homeCallback, loadCallback):
        wx.Panel.__init__(self, parent, wx.ID_ANY)

        self._backCallback = backCallback
        self._homeCallback = homeCallback
        self._loadCallback = loadCallback
        self.__current_model_id = None

        self.bBack = wx.Button(self, label=_("Back to Print View"))
        self.bHome = wx.Button(self, label=_("My Redooit models"))

        self.__static_line_top = wx.StaticLine(self)
        self.__static_text_bottom = wx.StaticText(self, label=_("Add models to your local library, then double click to load a file on the build plate"), size=(400, 40), style=wx.ALIGN_CENTER)
        # Create and update my_models_library tree
        self.tree = MyXMLTree(self, self.loadModelFromTree, self.deleteModelFromDrive)

        # Button Event Handler
        self.bBack.Bind(wx.EVT_BUTTON, self.OnBack)
        self.bHome.Bind(wx.EVT_BUTTON, self.OnHome)

        self.search = wx.SearchCtrl(self, size=(200, -1), style=wx.TE_PROCESS_ENTER)
        self.search.SetDescriptiveText(_("search"))
        self.search.ShowCancelButton(1)
        self.search_items = []

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.bBack, 0, wx.ALL | wx.EXPAND, 5)
        sizer.Add((-1, 10))  # empty space
        sizer.Add(self.__static_line_top, 0, wx.LEFT | wx.RIGHT | wx.EXPAND, 5)
        sizer.Add((-1, 10))  # empty space
        sizer.Add(self.__static_text_bottom, 0)

        # Search
        sizer.Add((-1, 5))  # empty space
        hSizer = wx.BoxSizer(wx.HORIZONTAL)
        hSizer.Add(self.search, 1, wx.EXPAND, 0)
        hSizer.Add(self.bHome, 0, wx.LEFT, 5)
        sizer.Add(hSizer, 0, wx.ALL | wx.EXPAND, 5)
        # Set search event bindings
        self.search.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, self.OnCancel)
        # self.search.Bind(wx.EVT_TEXT, self.OnDoSearch)
        self.search.Bind(wx.EVT_SEARCHCTRL_SEARCH_BTN, self.OnDoSearch)
        self.search.Bind(wx.EVT_TEXT_ENTER, self.OnDoSearch)
        self.search.Bind(wx.EVT_MENU, self.OnSearchMenu)
        # self.search.Bind(wx.EVT_MENU_RANGE, self.OnSearchMenu)

        # Library tree
        sizer.Add(self.tree, 1, wx.ALL | wx.EXPAND, 5)
        self.SetSizer(sizer)
        self.Layout()

        self.loadModelsTree()

    def OnBack(self, event):
        self._backCallback()

    def OnHome(self, event):
        self._homeCallback()

    def loadModelsTree(self):
        self.tree.LoadXML(profile.getModelsLibraryFile())

    def saveModelsTree(self):  # need to call this function in mainWindow OnClose event handler
        self.tree.SaveXML(profile.getModelsLibraryFile())

    @staticmethod
    def getNameOnly(pt):
        all_ex = meshLoader.loadAllExtensions()
        label = os.path.basename(pt)
        for e in all_ex:
            if label.find(e) != -1:
                label = label.replace(e, '')
        return label

    def addExternalModelInTree(self, m_title, ext_path):
        if self.modelNameExists(m_title):
            n = self.getUniqueName(m_title)
            mbInfoMessage = _("The item with the same name already exists in the library.")
            inInfoMessage = mbInfoMessage + " " + _("Please input another name.")
            while True:
                dlg = wx.TextEntryDialog(self, inInfoMessage, _("New Item Name"), defaultValue=n)
                dlg.ShowModal()
                n = dlg.GetValue()
                rc = dlg.GetReturnCode()
                dlg.Destroy()
                if rc != wx.ID_OK:
                    return
                else:
                    if self.modelNameExists(n):
                        dlg = wx.MessageDialog(self, mbInfoMessage, _("Warning"), wx.OK | wx.ICON_WARNING)
                        dlg.ShowModal()
                        dlg.Destroy()
                    else:
                        break
            u_title = n
        else:
            u_title = m_title
        m_id = "None"
        fn = os.path.basename(ext_path)
        u_path = webmodule.getUniqueFileName(profile.getModelsPath(), fn)
        try:
            shutil.copy(ext_path, u_path)
        except Exception as e:
            # print ": ".join((e.strerror, e.filename))
            dlg = wx.MessageDialog(self, e.__unicode__(), _("Error when copy file into library"), wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
            return
        self.tree.addItem(u_title, {'id': m_id, 'path': u_path})

    def addModelInTree(self, m_title, m_id="None", all_path=[]):
        if len(all_path) > 1:
            m_paths = {MyLibraryPanel.getNameOnly(pt): {'id': m_id, 'path': pt} for pt in all_path}
            self.tree.addItems(m_title, m_paths)
        elif len(all_path) == 1:
            m_path = ';'.join(all_path)
            self.tree.addItem(m_title, {'id': m_id, 'path': m_path})

    def updateCollectionInTree(self, col_structure):
        datas = self.tree.GetAllItemsData()
        all_items_paths = []
        for dt in datas:
            pt = dt.get('path')
            all_items_paths.append(pt)
        # find
        if isinstance(col_structure, dict):
            for col in col_structure:
                col_item = self.tree.get_or_create_subitem(col, "folder")
                thi_struct = col_structure[col]
                if isinstance(thi_struct, dict):
                    for thi in thi_struct:
                        thi_item = self.tree.get_or_create_subitem(thi, "folder", col_item)
                        f_struct = thi_struct[thi]
                        if isinstance(f_struct, dict):
                            for f_name in f_struct:
                                f_path = f_struct[f_name]
                                print col, thi, f_name, f_path
                                if f_path not in all_items_paths:
                                    # add file in collection
                                    f_item = self.tree.get_or_create_subitem(f_name,
                                                                               {'id': "None", 'path': f_path}, thi_item)
        pass

    def isAlredyExists(self, id):
        datas = self.tree.GetAllItemsData()
        exists = False
        for dt in datas:
            m_id = dt.get('id')
            if id == m_id:
                exists = True
                break
        return exists

    def modelNameExists(self, name):
        names = self.tree.GetAllItemsNames()
        return name in names

    def getUniqueName(self, name):
        names = self.tree.GetAllItemsNames()
        n = 1
        newname = name
        while newname in names:
            newname = "%s (%d)" % (name, n)
            n += 1
        return newname

    def checkModelExistsWithMD5(self, pth):
        datas = self.tree.GetAllItemsData()
        exists = False
        tMD5 = crypto.getMD5(pth)
        for dt in datas:
            dpth = dt.get('path')
            if dpth:
                if os.path.exists(dpth):
                    dMD5 = crypto.getMD5(dpth)
                    if tMD5 == dMD5:
                        exists = True
                        break
        return exists

    def getModelsByID(self, id):
        datas = self.tree.GetAllItemsData()
        models = []
        for dt in datas:
            m_id = dt.get('id')
            if id == m_id:
                pt = dt.get('path')
                if pt:
                    models.append(pt)
        return models

    def loadModelFromTree(self, data):
        m_path = data.get('path')
        all_path = m_path.split(';')
        self._loadCallback(all_path)

    def deleteModelFromDrive(self, data):
        m_path = data.get('path')
        if m_path:
            all_path = m_path.split(';')
            for pt in all_path:
                if os.path.isfile(pt):
                    os.remove(pt)
            if all_path:
                # remove folder only if is empty
                first_item = all_path[0]
                pf = os.path.dirname(first_item)
                if os.path.exists(pf):
                    if not os.listdir(pf):
                        os.rmdir(pf)

    def OnCancel(self, event):
        self.search.Clear()
        # self.search.SetMenu(None)
        self.search_items = []

    def OnDoSearch(self, event):
        items = self.tree.GetItemsByPattern(self.search.GetValue())
        self.search_items = []
        if items:
            search_menu = wx.Menu()
            for i, item in enumerate(items):
                item_data = self.tree.GetItemData(item).GetData()
                item_name = self.tree.GetItemText(item)
                search_menu.Append(i, item_name)
                self.search_items.append(item_data)
        else:
            search_menu = None
        # self.search.SetMenu(search_menu)
        if search_menu:
            self.ShowSearchPopupMenu(search_menu)

    def OnSearchMenu(self, event):
        print event.GetId()
        data = self.search_items[event.GetId()]
        self.loadModelFromTree(data)

    def ShowSearchPopupMenu(self, menu):
        rect = self.search.GetRect()
        self.search.PopupMenu(menu, (0, rect.Height))
        # rect = self.search.GetRect()
        # self.PopupMenu(menu, (rect.Left, rect.Bottom))
        # popup = self.FindFocus()
        # print popup


class MyXMLTree(wx.TreeCtrl):
    def __init__(self, parent, selected_item_cb, item_removed_cb):
        # |wx.TR_HAS_BUTTONS # |wx.TR_HIDE_ROOT # |wx.TR_EDIT_LABELS
        wx.TreeCtrl.__init__(self, parent, style=wx.TR_DEFAULT_STYLE | wx.TR_SINGLE | wx.TR_EDIT_LABELS)

        self.__draggingItem = None
        self.__default_item_name = _('New Folder')
        self.__selectedItemCB = selected_item_cb
        self.__itemRemovedCB = item_removed_cb
        self.__default_item_bg_color = None
        self.__cant_drag_item_bg_color = wx.Colour(255, 130, 110)

        # Drag-N-Drop Event Handlers
        self.Bind(wx.EVT_TREE_BEGIN_DRAG, self.OnBeginDrag)
        self.Bind(wx.EVT_TREE_END_DRAG, self.OnEndDrag)
        # Context Menu for selected item Event Handler
        self.Bind(wx.EVT_TREE_ITEM_MENU, self.OnItemMenu)
        # self.Bind(wx.EVT_CONTEXT_MENU, self.OnContextMenu)
        # If item double clicked
        self.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.OnDblclickItem)
        # event handler for mouse capture realisation of Drag and Drop
        # self.Bind(wx.EVT_MOTION, self.followDrag)

        # Cursors for dragging items
        self.dragCursor = wx.StockCursor(wx.CURSOR_ARROW)
        self.badDragCursor = wx.StockCursor(wx.CURSOR_NO_ENTRY)
        self.defaultCursor = wx.StockCursor(wx.CURSOR_ARROW)
        self.__currCursor = self.defaultCursor
        self.SetCursor(self.defaultCursor)

    ###
    # Item Manipulations
    ###
    def addItem(self, title, data):
        if title and isinstance(data, dict):
            item = self.GetRootItem()
            if item.IsOk():
                new_treeItem = self.AppendItem(item, title, data=wx.TreeItemData(data))

    def addItems(self, title, datalist):
        if title and isinstance(datalist, dict):
            item = self.GetRootItem()
            if item.IsOk():
                new_treeItem = self.AppendItem(item, title, data=wx.TreeItemData({'editable': 'True'}))
                for key, value in datalist.items():
                    if isinstance(value, dict):
                        self.AppendItem(new_treeItem, key, data=wx.TreeItemData(value))
                    else:
                        print 'error append subitem'
                self.Expand(new_treeItem)

    def get_or_create_subitem(self, title, data, item=None):
        folder_item = isinstance(data, str) or isinstance(data, unicode)
        element_item = isinstance(data, dict)
        if not folder_item and not element_item:
            raise ValueError("Argument 'data' does not contains parameters for creating/getting folder or element.")

        if not item:
            item = self.GetRootItem()
        if item.IsOk():
            items = self.GetSubItemsWithTitle(item, title)
            if items:
                ret_item = items[0]
            else:
                if folder_item:
                    data = {'editable': 'True'}
                ret_item = self.AppendItem(item, title, data=wx.TreeItemData(data))
        else:
            raise Exception("Not valid item")
        return ret_item



    ###
    # Folder Manipulations
    ###
    def OnRenameFolder(self, event):
        item = self.GetSelection()
        if item and item.IsOk():
            self.EditLabel(item)

    def OnAddSubFolder(self, event):
        item = self.GetSelection()
        if item and item.IsOk():
            new_treeItem = self.AppendItem(item, self.__default_item_name, data=wx.TreeItemData({'editable': 'True'}))

    def OnRemoveItem(self, event):
        item = self.GetSelection()
        if item and item.IsOk():
            if callable(self.__itemRemovedCB):
                if self.ItemHasChildren(item):
                    self.__removeFolderItems(item)
                else:
                    self.__removeItem(item)
            self.Delete(item)

    ###
    # double click on item (if folder then collapse/expand else open model file in sceneViewer)
    ###
    def OnDblclickItem(self, event):
        item = event.GetItem()
        if self.IsFolderItem(item):
            if self.IsExpanded(item):
                self.Collapse(item)
            else:
                self.Expand(item)
        else:
            idata = self.GetItemData(item)
            if idata:
                data = idata.GetData()
                if isinstance(data, dict):
                    self.__selectedItemCB(data)

    ###
    # Item Context Menu (Only for folder Items)
    ###
    def OnItemMenu(self, event):
        item = event.GetItem()
        self.SelectItem(item)
        menu = wx.Menu()
        if self.IsFolderItem(item):
            # can't rename root item
            if self.GetRootItem() != item:
                mi = menu.Append(wx.ID_EDIT, 'Rename Folder')
                self.Bind(wx.EVT_MENU, self.OnRenameFolder, mi)
                menu.AppendSeparator()
            mi = menu.Append(wx.ID_ADD, 'Add Sub Folder')
            self.Bind(wx.EVT_MENU, self.OnAddSubFolder, mi)
            # can't remove root folder and folder with sub items
            if self.GetRootItem() != item:
                mi = menu.Append(wx.ID_DELETE, 'Remove Folder')
                self.Bind(wx.EVT_MENU, self.OnRemoveItem, mi)
        else:
            mi = menu.Append(wx.ID_DELETE, 'Remove Model')
            self.Bind(wx.EVT_MENU, self.OnRemoveItem, mi)
        # show popup menu
        self.PopupMenu(menu)

    ###
    # Drag and Drop items
    ###
    def OnBeginDrag(self, event):
        item = event.GetItem()
        if item == self.GetRootItem():
            return
        if not self.IsEditableItem(item):
            return

        self.__draggingItem = item
        event.Allow()

        self.SetCursor(self.dragCursor)
        self.__currCursor = self.dragCursor
        if platform.system().lower() != 'darwin':
            self.Bind(wx.EVT_MOUSE_EVENTS, self.followDrag)  # need for changing cursor icon
        self.enableDropsForItem(item)
        # if self.HasCapture():
        #     self.ReleaseMouse()
        # self.CaptureMouse()

    def followDrag(self, event):
        if self.__currCursor == self.defaultCursor:
            return
        itemSrc = self.__draggingItem
        pos = event.GetPosition()
        (itemDst, hit_flags) = self.HitTest(pos)
        goodDrag = self.CanDraggedInto(itemSrc, itemDst)
        wx.CallAfter(self.__changeCursor, goodDrag)

    def __changeCursor(self, goodDrag):
        if goodDrag:
            if self.__currCursor != self.dragCursor:
                self.SetCursor(self.dragCursor)
                self.__currCursor = self.dragCursor
        else:
            if self.__currCursor != self.badDragCursor:
                self.SetCursor(self.badDragCursor)
                self.__currCursor = self.badDragCursor

    def OnEndDrag(self, event):
        itemSrc = self.__draggingItem
        itemDst = event.GetItem()
        self.__draggingItem = None

        if platform.system().lower() != 'darwin':
            self.Bind(wx.EVT_MOUSE_EVENTS, None)
        self.disableDropsForItem()
        self.SetCursor(self.defaultCursor)
        self.__currCursor = self.defaultCursor
        # if self.HasCapture():
        #     self.ReleaseMouse()

        if self.CanDraggedInto(itemSrc, itemDst):
            self.__moveItem(itemSrc, itemDst)

    ###
    # move itemSrc with subitems in position after itemDst
    ###
    def __moveItem(self, itemSrc, itemDst):
        # itemPar = None if self.ItemHasChildren(itemDst) else self.GetItemParent(itemDst)
        itemPar = None if self.IsFolderItem(itemDst) else self.GetItemParent(itemDst)

        title = self.GetItemText(itemSrc)
        data = self.GetItemData(itemSrc).GetData()
        if itemPar:
            newItem = self.InsertItem(itemPar, itemDst, title, data=wx.TreeItemData(data))
        else:
            newItem = self.PrependItem(itemDst, title, data=wx.TreeItemData(data))
            # newItem = self.InsertItemBefore(itemDst, 0, title, data=wx.TreeItemData(data))
        if self.IsFolderItem(itemSrc):
            self.__moveFolderItems(itemSrc, newItem)
            self.ExpandAllChildren(newItem)
        self.Delete(itemSrc)

    def __moveFolderItems(self, itemSrc, itemDst):
        child_itemSrc, cookie = self.GetFirstChild(itemSrc)
        while child_itemSrc.IsOk():
            title = self.GetItemText(child_itemSrc)
            data = self.GetItemData(child_itemSrc).GetData()
            child_itemDst = self.AppendItem(itemDst, title, data=wx.TreeItemData(data))
            if self.ItemHasChildren(child_itemSrc):
                self.__moveFolderItems(child_itemSrc, child_itemDst)
            child_itemSrc, cookie = self.GetNextChild(itemSrc, cookie)

    ###
    # for Drag and Drop
    ###
    def disableDropsForItem(self):
        if not self.__default_item_bg_color:
            self.__default_item_bg_color = self.GetItemBackgroundColour(self.GetRootItem())
        items = self.GetAllItems()
        for i in items:
            self.SetItemBackgroundColour(i, self.__default_item_bg_color)

    def enableDropsForItem(self, item):
        items = self.GetAllItems()
        for i in items:
            if not self.CanDraggedInto(item, i):
            #     self.SetItemDropHighlight(i, highlight=True)
            # else:
            #     self.SetItemDropHighlight(i, highlight=False)
                self.SetItemBackgroundColour(i, self.__cant_drag_item_bg_color)

    ###
    # Generate tree from loaded xml
    ###
    def __xmlToTree(self, xmlElem, treeItem):
        for xmlChild in xmlElem:
            data = xmlChild.attrib
            title = data.pop('title')
            new_treeItem = self.AppendItem(treeItem, title, data=wx.TreeItemData(data))
            if xmlChild.tag == 'Folder':
                # self.SetItemHasChildren(new_treeItem, True)  # For NEW EDITION: Folder Items Need Have This parameter
                self.__xmlToTree(xmlChild, new_treeItem)

    def LoadXML(self, filename=None):
        if os.path.exists(filename):
            tree = ET.parse(filename)
            xmlElem = tree.getroot()
        else:  # loading template if file doesen't exists
            xmlElem = ET.fromstring('<MyModelsLibrary title="My Models Library" editable="False"/>')

        if xmlElem.tag == 'MyModelsLibrary':
            data = xmlElem.attrib
            title = data.pop('title')
            treeItem = self.AddRoot(title, data=wx.TreeItemData(data))
            self.__xmlToTree(xmlElem, treeItem)
            self.ExpandAll()
        else:
            print "Error file loading: Incorrect file"

    ###
    # Generate xml file from tree items with data and save it
    ###
    def __treeToXML(self, treeItem, xmlElem):
        child_treeItem, cookie = self.GetFirstChild(treeItem)
        while child_treeItem.IsOk():
            data = self.GetItemData(child_treeItem).GetData()
            title = self.GetItemText(child_treeItem)
            data['title'] = title
            # if self.ItemHasChildren(child_treeItem):
            if self.IsFolderItem(child_treeItem):
                child_xmlElem = ET.SubElement(xmlElem, 'Folder', data)
                self.__treeToXML(child_treeItem, child_xmlElem)
            else:
                child_xmlElem = ET.SubElement(xmlElem, 'Model', data)
            child_treeItem, cookie = self.GetNextChild(treeItem, cookie)

    def SaveXML(self, filename):
        treeItem = self.GetRootItem()
        title = self.GetItemText(treeItem)
        data = self.GetItemData(treeItem).GetData()
        data['title'] = title
        xmlElem = ET.Element('MyModelsLibrary')
        for datakey, dataval in data.items():
            xmlElem.attrib[datakey] = dataval
        self.__treeToXML(treeItem, xmlElem)
        # print ET.tostring(xmlElem)
        pXML = prettify(xmlElem)
        f = open(filename, 'w')
        f.write(pXML)
        f.close()

    ###
    # Additional testings
    ###
    def CanDraggedInto(self, itemSrc, itemDst):
        dragged = False
        if itemDst.IsOk():
            dragged = True
            if self.ItemHasChildren(itemSrc):
                dragged = not self.IsSubitemOfItem(itemDst, itemSrc)
            if itemSrc == itemDst:
                dragged = False
        return dragged

    def IsSubitemOfItem(self, subitem, item):
        res = False
        ### faster edition with using GetItemParent for subitem
        rootItem = self.GetRootItem()
        if rootItem != subitem:
            paritem = self.GetItemParent(subitem)
            while rootItem != paritem:
                if item == paritem:
                    res = True
                    break
                paritem = self.GetItemParent(paritem)
        return res

    def IsFolderItem(self, item):
        itemData = self.GetItemData(item)
        assert isinstance(itemData, wx.TreeItemData)
        data = itemData.GetData()
        assert data != None
        return data.get('editable') is not None

    def IsEditableItem(self, item):
        itemData = self.GetItemData(item)
        data = itemData.GetData()
        if data == None:
            return False
        editable = data.get('editable')
        if editable == None:
            return True
        ret = None
        if isinstance(editable, bool):
            ret = editable
        elif hasattr(editable, 'lower'):
            ret = editable.lower() == 'true'
        assert ret != None
        return ret

    ###
    # Getting All Items
    ###
    def GetAllItemsData(self):
        items = self.GetAllItems()
        datas = []
        for item in items:
            datas.append(self.GetItemData(item).GetData())
        return datas

    def GetAllItemsNames(self):
        items = self.GetAllItems()
        return [self.GetItemText(item) for item in items]

    def GetAllItems(self):
        root = self.GetRootItem()
        arr = [root]
        self.__walkSubItems(root, arr)
        return arr

    def GetSubItemsWithTitle(self, item, title):
        ret_items = []
        child_item, cookie = self.GetFirstChild(item)
        while child_item.IsOk():
            if self.GetItemText(child_item) == title:
                ret_items.append(child_item)
            child_item, cookie = self.GetNextChild(item, cookie)
        return ret_items

    def GetItemsByPattern(self, pattern):
        found = []
        if len(pattern) < 1:
            return found

        items = self.GetAllItems()
        for item in items:
            itemName = self.GetItemText(item)
            match = re.search(pattern, itemName, re.I)
            if match:
                found.append(item)

        return found

    def __walkSubItems(self, item, arr):
        child_item, cookie = self.GetFirstChild(item)
        while child_item.IsOk():
            arr.append(child_item)
            if self.ItemHasChildren(child_item):
               self.__walkSubItems(child_item, arr)
            child_item, cookie = self.GetNextChild(item, cookie)

    ###
    # Additional functions with callbacks for removing items
    ###
    def __removeFolderItems(self, item):
        child_item, cookie = self.GetFirstChild(item)
        while child_item.IsOk():
            if self.ItemHasChildren(child_item):
               self.__removeFolderItems(child_item)
            else:
                self.__removeItem(child_item)
            child_item, cookie = self.GetNextChild(item, cookie)

    def __removeItem(self, item):
        dt = self.GetItemData(item).GetData()
        self.__itemRemovedCB(dt)