__author__ = 'Igor Tkalenko'

import wx
import os
import webbrowser
from wx.lib import scrolledpanel

from Cura.util import profile
from Cura.gui import configBase
from Cura.util import webmodule

class loginPanel(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent,-1)

		sizer = wx.GridBagSizer(2, 2)
		self.SetSizer(sizer)

		self.emailCtrl = wx.TextCtrl(self, -1, '')
		self.passwordCtrl = wx.TextCtrl(self, -1, '', style=wx.TE_PASSWORD)
		self.loginButton = wx.Button(self, -1, _('Login'), style=wx.BU_EXACTFIT)

		configBase.TitleRow(self, 'redooit.com '+_('Authentication'))
		self.__addRow(_('Email'), self.emailCtrl, flag=wx.EXPAND)
		self.__addRow(_('Password'), self.passwordCtrl, flag=wx.EXPAND)

		sizer.Add(self.loginButton, (sizer.GetRows(),1), span=(1,1), border=5, flag=wx.EXPAND)

		sizer.AddGrowableCol(1)

		self.Bind(wx.EVT_BUTTON, self.OnLogin, self.loginButton)
		self.updateLoginPanel()

	def __addRow(self, labelStr, control, flag = 0):
		sizer = self.GetSizer()
		x = sizer.GetRows()
		y = 0

		labelCtrl = wx.lib.stattext.GenStaticText(self, -1, labelStr)
		# self.label.Bind(wx.EVT_ENTER_WINDOW, self.OnMouseEnter)
		# self.label.Bind(wx.EVT_LEAVE_WINDOW, self.OnMouseExit)

		sizer.Add(labelCtrl, (x,y), flag=wx.ALIGN_CENTER_VERTICAL|wx.LEFT,border=10)
		sizer.Add(control, (x,y+1), flag=wx.ALIGN_CENTER_VERTICAL|flag)

	def OnLogin(self, e):
		if webmodule.isConnected():
			err, message = webmodule.disconnect()
			if err:
				dlg = wx.MessageDialog(self, message, _("Logout"), wx.OK | wx.ICON_INFORMATION)
				dlg.ShowModal()
				dlg.Destroy()
			self.updateLoginPanel()
			self.emailCtrl.SetFocus()
			return None

		username = self.emailCtrl.GetValue()
		password = self.passwordCtrl.GetValue()
		err, message = webmodule.getAccessToken(username, password)
		if not err:
			self.emailCtrl.SetValue('')
			self.passwordCtrl.SetValue('')

		self.updateLoginPanel()

		if message.lower().find("wrong username or password") == 0:
			flag = wx.ICON_ERROR
		else:
			flag = wx.ICON_INFORMATION
		dlg = wx.MessageDialog(self, message, _("Login"), wx.OK | flag)
		dlg.ShowModal()
		dlg.Destroy()

	def updateLoginPanel(self):
		if webmodule.isConnected():
			self.loginButton.SetLabel(_("Renew Authentication"))
			self.emailCtrl.Enabled = False
			self.passwordCtrl.Enabled = False
		else:
			self.loginButton.SetLabel(_("Login"))
			self.emailCtrl.Enabled = True
			self.passwordCtrl.Enabled = True
