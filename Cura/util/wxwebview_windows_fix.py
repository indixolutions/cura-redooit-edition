__author__ = 'Igor Tkalenko'


### set IE version for wx.html2.WebView control
import sys
if sys.platform == 'win32':
    import _winreg
    EXPLORER_PATH = "SOFTWARE\\Microsoft\\Internet Explorer"
    FEATURE_BROWSER_EMULATION = "Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION"

    def get_wx_IE_version():
        try:
            handle = _winreg.OpenKey(_winreg.HKEY_CURRENT_USER, FEATURE_BROWSER_EMULATION)
            (value, type) = _winreg.QueryValueEx(handle, 'python.exe')
        except:
            value = None
        return value

    def get_IE_version():
        vers_value = 7000
        try:
            handle = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, EXPLORER_PATH)
        except:
            print "Can't get IE version from registry"
        else:
            try:
                (vers_value, type) = _winreg.QueryValueEx(handle, 'Version')
                vers_value = int(vers_value.split('.')[0])
            except:
                vers_value = 0
            try:
                (svc_vers_value, type) = _winreg.QueryValueEx(handle, 'svcVersion')
                svc_vers_value = int(svc_vers_value.split('.')[0])
            except:
                svc_vers_value = 0
            value = max([vers_value, svc_vers_value])
            if value == 8:
                vers_value = 8888
            elif value == 9:
                vers_value = 9999
            elif value == 10:
                vers_value = 10001
            elif value == 11:
                vers_value = 11001
        return vers_value

    def set_wx_IE_version(vers_value, program):
        try:
            key = _winreg.CreateKey(_winreg.HKEY_CURRENT_USER, FEATURE_BROWSER_EMULATION)
            _winreg.SetValueEx(key, program, 0, _winreg.REG_DWORD, vers_value)
        except:
            print "can't set registry key for WebView control"

    ie_ver = get_IE_version()
    wx_ie = get_wx_IE_version()
    if wx_ie != ie_ver:
        set_wx_IE_version(ie_ver, 'python.exe')
        print "set_wx_version"