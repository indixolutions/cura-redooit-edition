__author__ = 'Igor Tkalenko'

# import struct
from Crypto.Cipher import AES
from io import BytesIO
from Cura.util import webmodule
import base64
import os

class DecodeError(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __str__(self):
        return repr(self.msg)

def dec_open(filename, enc=None):
    if enc == None:
        enc = filename.lower()[-4:] == '.a3d'
    if enc:
        model_id = os.path.split(os.path.dirname(filename))[-1]  # Folder name contains model file is model_id
        s = webmodule.getSecret(model_id)
        if s:
            key = s.get('key')
            iv = s.get('vector')
            if key and iv:
                f = decrypt_on_the_fly(key, base64.b64decode(iv), filename)
            else:
                error = s.get('error')
                if not error:
                    error = _('Not found a key and/or vector')
                raise DecodeError(error)
        else:
            return None
    else:
        f = open(filename, 'rb')
    return f


class decrypt_on_the_fly(object):
    def __init__(self, key, iv, filename, cb_progress=None, cache_size=1024*1024):
        f = open(filename, 'rb')

        # temp solution for getting total size
        f.seek(0, os.SEEK_END)
        origsize = f.tell()
        f.seek(0)
        # origsize = struct.unpack('<Q', f.read(struct.calcsize('Q')))[0]

        self._total_size = origsize
        self.__readed_size = 0
        self._cache_size = cache_size
        self.__f = f
        self.__cache = BytesIO()
        self.__decryptor = AES.new(key, AES.MODE_CBC, iv)

        self._cb_progress = cb_progress
        self._progress = 0
        self._inv_total = 100.0 / float(self._total_size)
        self._current_percent = -1

    def __iter__(self):
        return self

    def next(self):
        part = self.__cache.readline()
        if not part:
            if self.update_cache():
                part = self.__cache.readline()
        if part:
            if part.find('\n') == -1:  # iflast readed line
                if self.update_cache():
                    part += self.__cache.readline()
        else:
            raise StopIteration
        return part

    def update_cache(self):
        r = self.__f.read(self._cache_size)
        if r:
            dec = self.__decryptor.decrypt(r)
            self.__cache.seek(0)
            self.__cache.write(dec)
            lendec = len(dec)
            self.__readed_size += lendec
            if lendec != self._cache_size:
                self.__cache.truncate(lendec)
                # self.__cache.truncate(self._total_size % self._cache_size)
            self.__cache.seek(0)
        return bool(r)

    def __del__(self):
        self.close()
        del self.__f
        del self.__cache

    def __len__(self):
        return self._total_size

    def close(self):
        if not self.__f.closed:
            self.__f.close()
        if not self.__cache.closed:
            self.__cache.close()

    def read(self, n=-1):
        part = self.__cache.read(n)
        while len(part) < n:
            if self.update_cache():
                part += self.__cache.read(n - len(part))
            else:
                break

        self._progress += int(len(part))
        perc = int(self._progress * self._inv_total)
        if self._current_percent != perc:
            self._current_percent = perc
            if callable(self._cb_progress):
                self._cb_progress(perc)
        return part
