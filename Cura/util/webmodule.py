__author__ = 'Igor Tkalenko'

import json
import urllib
import urllib2
from urlparse import urlparse
from Cura.util import profile
from Cura.util import crypto
from Cura.gui import threadingDialog as td
from uuid import getnode as getDeviceID
import os

opener = urllib2.build_opener(urllib2.HTTPSHandler())  # , urllib2.HTTPRedirectHandler()
opener.addheaders = [
    ('User-agent', 'Mozilla/5.0'),
]

website = ""
tokenValue = ""
CONNECTION_ERROR = 'connection problem'

CONNECTION_ERROR_MSG = ""

def loadDefaultMessages():
    global CONNECTION_ERROR_MSG, REDOOIT_AUTHENTICATION_ERROR_MSG
    CONNECTION_ERROR_MSG = _("You need to be connected to the internet to view this model file. Please, check your network connection and try again.")
    REDOOIT_AUTHENTICATION_ERROR_MSG = _("You need to be authenticated with your redooit.com credentials in order to complete this request")

l_cb = None
def set_login_callback(login_cb):
    global l_cb
    l_cb = login_cb


def getUniqueFileName(path, name):
    tmpname = os.path.join(path, name)
    if os.path.exists(tmpname):
        if '.' not in name:
            ext = ''
            fbase = name
        else:
            ext = '.' + name.split('.')[-1]
            fbase = name[:-len(ext)]
        inc = len([x for x in os.listdir(path) if x.startswith(fbase)])
        while True:
            newname = "%s_%d%s" % (fbase, inc, ext)
            tmpname = os.path.join(path, newname)
            if not os.path.exists(tmpname):
                break
            inc += 1
    return tmpname


def getContentType(content):
    return ""


def isPathToSingleModel(url):
    r = urlparse(url)
    pt = r.path
    return ('/model/' in pt) or ('/model-component/' in pt)

def encodedDict(in_dict):
    out_dict = {}
    for k, v in in_dict.iteritems():
        if isinstance(v, unicode):
            v = v.encode('utf8')
        elif isinstance(v, str):
            v = v.decode('utf8')
        out_dict[k] = v
    return out_dict


def getFullUrl(url):
    global website
    return "%s%s" % (website, url)


def createGetRequest(full_url, values=None, needToken=False):
    data = {}
    if needToken:
        global tokenValue
        data["accessToken"] = tokenValue
    if isinstance(values, dict):
        data.update(encodedDict(values))
    data = urllib.urlencode(data)
    if data:
        full_url = "{}?{}".format(full_url, data)
    req = urllib2.Request(full_url, None)
    # print("%s %s %s" % (str(req.get_method()), str(req.get_full_url()), str(req.header_items())))
    return req


def createPostRequest(full_url, values=None, needToken=False):
    data = {}
    if needToken:
        global tokenValue
        data["accessToken"] = tokenValue
    if isinstance(values, dict):
        data.update(encodedDict(values))
    data = urllib.urlencode(data)
    req = urllib2.Request(full_url, data)
    # print("%s %s %s" % (str(req.get_method()), str(req.get_full_url()), str(req.header_items())))
    return req


def sendRequest(request):
    result = ""
    try:
        response = opener.open(request, timeout=60)
    except urllib2.URLError as e:
        if hasattr(e, 'reason'):
            # print('Reason: %s' % str(e.reason))
            pass
        if hasattr(e, 'code'):
            # print('Error code: %s' % str(e.code))
            result = e.read()
        else:
            result = CONNECTION_ERROR
    except:
        # print('exception in sending Request')
        pass
    else:
        result = response
    return result

def isResponse(resp):
    return hasattr(resp, 'headers') and hasattr(resp, 'read')

def isZipFile(resp):
    zipped = False
    try:
        zipped = "zip" in resp.headers.get("Content-Type", "")
    except:
        pass
    return zipped


def jsonToDict(page):
    js = {}
    # cut only json data from page
    fpage, tpage = page.find('{'), page.rfind('}')
    if fpage != -1 and tpage != -1:
        jspage = page[fpage:tpage+1]
        try:
            js = json.loads(jspage)
        except:
            pass
    return js


def getAccessToken(username, password):
    global tokenValue, l_cb
    if tokenValue:
        disconnect()
    login_url = getFullUrl("/user/login")
    parameters = {"username": username, "password": password, "deviceID": getDeviceID()}
    loginReq = createPostRequest(login_url, parameters)
    if not loginReq:
        return 1, _("Problem with request creation")
    response = sendRequest(loginReq)
    if isResponse(response):
        resultJson = response.read()
    else:
        if response:
            resultJson = response
        else:
            return 2, CONNECTION_ERROR_MSG
    resultDict = jsonToDict(resultJson)
    if not resultDict:
        if response:
            return 2, CONNECTION_ERROR_MSG
        else:
            return 3, _("Returned value is not a json")
    accessToken = resultDict.get("accessToken")
    if not accessToken:
        err = resultDict.get("error")
        if err is None:
            err = str(resultDict)
        return 4, err
    tokenValue = accessToken
    if callable(l_cb):
        l_cb()
    saveToken()
    return 0, _("You are connected")


def isConnected():
    global tokenValue
    return bool(tokenValue)

def getSiteToken():
	global tokenValue
	return tokenValue


def testInternetConnection():
    url = getFullUrl('')
    try:
        response=urllib2.urlopen(url, timeout=10)
        return True
    except urllib2.URLError as err: pass
    return False


def disconnect():
    global tokenValue, l_cb
    err = True
    msg = ""
    url = getFullUrl("/user/logout")
    req = createPostRequest(url, needToken=True)
    res = sendRequest(req)
    if isResponse(res):
        value = res.read()
        js = jsonToDict(value)
        if js.get("accessToken") == "":
            tokenValue = ""
            saveToken()
            err = False
    elif res:
        js = jsonToDict(res)
        msg = js.get('error')
    if callable(l_cb):
        l_cb()
    if not msg and err:
        msg = CONNECTION_ERROR_MSG
    return err, msg


def loadPreferences():
    global tokenValue, website
    website = profile.getWebPreference("siteUrl")
    encToken = profile.getWebPreference("siteToken")
    if encToken:
        tokenValue = crypto.decrypt(encToken)


def saveToken():
    global tokenValue
    encToken = crypto.encrypt(tokenValue)
    profile.putWebPreference("siteToken", encToken)


def getSecret(model_id):
    url = getFullUrl("/secret/{}".format(model_id))
    req = createPostRequest(url, needToken=True)
    res = sendRequest(req)
    if isResponse(res):
        value = res.read()
        js = jsonToDict(value)
    elif res:
        if res == CONNECTION_ERROR:
            js = {'error': CONNECTION_ERROR_MSG}
        else:
            js = jsonToDict(res)
    else:
        js = {'error': CONNECTION_ERROR_MSG}
    if not js:
        js = {'error': _("Returned value is not json")}
    return js


def getModelByID(model_id):
    url = getFullUrl("/model/{}".format(model_id))
    req = createGetRequest(url, needToken=True)
    res = sendRequest(req)
    if isResponse(res):
        value = res.read()
        js = jsonToDict(value)
        return js
    return None


def getThingiverseAuthToken(code):
    url = getFullUrl("/thingiverse/getAccessToken")
    req = createPostRequest(url, {"code": code}, needToken=True)
    res = sendRequest(req)
    if isResponse(res):
        value = res.read()
        js = jsonToDict(value)
        if isinstance(js, dict):
            err = js.get('error')
            token = js.get('accessToken')
            if err:
                raise Exception(err)
            if token:
                return token
        else:
            raise Exception(_("Returned value is not json"))
    else:
        raise Exception(res)
    return ""  # res



def downloadModelByID(model_id, parent=None, callback=None):
    # print("downloadModelByID %s" % model_id)
    url = getFullUrl("/download/{}".format(model_id))
    req = createPostRequest(url, needToken=True)
    res = sendRequest(req)
    result = False
    if isResponse(res):
        if res.headers.get("Content-Type", ""):
            download_path = profile.getModelsPath()
            download_file = os.path.extsep.join((url.split('/')[-1], 'model'))
            # download_fp = getUniqueFileName(download_path, download_file)
            download_fp = os.path.join(download_path, download_file)

            thr = td.DownloadThread(res, download_fp)
            dlg = td.TDProgressDialog("Download Dialog", 'Downloading')
            res_d = dlg.ShowModal()
            # print 'downloading progress dialog closed. Result %s' % str(res_d)
            thr_res = thr.getResult()
            del thr
            # print "downloading thread deleted"
            ## need info if closing or error and break from function

            iszip = False
            if thr_res == td.THR_ERROR:
                readed = res.read()
                resultDict = jsonToDict(readed)
                result = resultDict.get('error')
                # print "downloading error: %s" % str(resultDict)
            elif thr_res != td.THR_CANCEL:
                if isZipFile(res):
                    iszip = True
                    df_fn, df_ex = os.path.splitext(download_file)
                    extract_path = os.path.join(download_path, df_fn)
                    if not os.path.exists(extract_path):
                        os.mkdir(extract_path)
                    elif os.path.isfile(extract_path):
                        os.remove(extract_path)

                    thr = td.UnpackingThread(download_fp, extract_path)
                    dlg = td.TDProgressDialog("Unpacking Dialog", 'Unpacking')
                    res_d = dlg.ShowModal()
                    # print "unpacking progress dialog closed. Result %s" % str(res_d)
                    thr_res = thr.getResult()
                    del thr
                    # print "unpacking thread deleted"
                    if thr_res == td.THR_ERROR:
                        result = '%s %s' % (_('Error extracting archive'), download_file)
                        # print "unpacking error: %s" % str(result)
                    elif thr_res != td.THR_CANCEL:
                        result = thr_res
                else:
                    result = [download_fp]
            if thr_res == td.THR_ERROR or thr_res == td.THR_CANCEL or (iszip and thr_res == result):
                if os.path.exists(download_fp):
                    os.remove(download_fp)
                    # print "downloaded file removed"
        else:
            readed = res.read()
            resultDict = jsonToDict(readed)
            result = resultDict.get('error')
            # print "Request sending error: %s" % str(resultDict)
    else:
        resultDict = jsonToDict(res)
        result = resultDict.get('error')
        if result:
            # print "Error: %s" % str(resultDict)
            pass
        elif resultDict:
            # print "No content type: %s" % str(resultDict)
            result = resultDict
        else:
            result = res

    if callable(callback):
        callback(result)

