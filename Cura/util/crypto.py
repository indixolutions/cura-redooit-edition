__author__ = 'Igor Tkalenko'


import base64
import hashlib

### simple encryption/decryption functions will be updated in the corresponded Milestone
def encrypt(data):
    try:
        encoded = base64.b64encode(data)
    except:
        encoded = ""
    return encoded

def decrypt(encoded):
    try:
        data = base64.b64decode(encoded)
    except:
        data = ""
    return data

def getMD5(afile, blocksize=65536):
    ofile = open(afile, 'rb')
    hasher = hashlib.md5()
    buf = ofile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = ofile.read(blocksize)
    ofile.close()
    return hasher.digest()