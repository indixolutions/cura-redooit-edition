__author__ = 'Igor Tkalenko'

import json
import urllib
import urllib2

from Cura.util import profile
from Cura.util import crypto
from Cura.util.webmodule import createGetRequest, createPostRequest, jsonToDict, isResponse,\
    getUniqueFileName, getThingiverseAuthToken


import cookielib
cookie = cookielib.CookieJar()

thingiverse_opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookie), urllib2.HTTPSHandler(), urllib2.HTTPRedirectHandler())
thingiverse_opener.addheaders = [
    ('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36'),
    # ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),
    # ('Accept', 'application/json, text/javascript, */*; q=0.01'),
    # ('Accept-Encoding', 'gzip, deflate, sdch'),
    # ('Accept-Language', 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4'),
    # ('X-NewRelic-ID', 'UQMGU1NACwsIXFBQ')
    # ('X-Requested-With', 'XMLHttpRequest')
    # ('Origin', 'https://accounts.makerbot.com')

    # ('Connection', 'keep-alive'),  # For using persistent connection
    # ('Keep-Alive', '60'),  # Timeout of connection
]


__all__ = ['', ]


ACCOUNT_LOGIN_URL = "https://accounts.makerbot.com/login"
AUTORIZE_URL = "https://www.thingiverse.com/login/oauth/authorize"
THINGIVERSE_TOKEN_URL = "https://www.thingiverse.com/login/oauth/access_token"
THINGIVERSE_API_URL = "https://api.thingiverse.com"

CONNECTION_ERROR_MSG = ""

def loadDefaultMessages():
    global CONNECTION_ERROR_MSG
    CONNECTION_ERROR_MSG = _("Internet connection problem. Please, check your network connection and try again.")


my_client_id = "cc70a235ccbd784af582"
access_token = ""


def getThingiverseAuthorisePage():
    global AUTORIZE_URL, my_client_id
    req = createGetRequest(AUTORIZE_URL, {"client_id": my_client_id})
    if not req:
        return AUTORIZE_URL
    return req.get_full_url()

def loadPreferences():
    global access_token
    encToken = profile.getWebPreference("thingiverse_token")
    if encToken:
        tokenValue = crypto.decrypt(encToken)
        if tokenValue:
            access_token = tokenValue


def save_token():
    global access_token
    encToken = crypto.encrypt(access_token)
    profile.putWebPreference("thingiverse_token", encToken)


def check_token():
    global access_token
    return bool(access_token)


def get_username():
    username = ''
    global access_token
    if access_token:
        url = "%s/users/me" % THINGIVERSE_API_URL
        req = createGetRequest(url, {'access_token': access_token})
        res = sendRequest(req)
        if isResponse(res):
            page = res.read()
            try:
                js = jsonToDict(page)
            except:
                pass
            else:
                username = js.get(u'name')
        else:
            raise Exception(res)
    return username


def sendRequest(request):
    result = ""
    try:
        response = thingiverse_opener.open(request, timeout=60)
    except urllib2.URLError as e:
        if hasattr(e, 'reason'):
            # print('Reason: %s' % str(e.reason))
            pass
        if hasattr(e, 'code'):
            # print('Error code: %s' % str(e.code))
            result = e.read()
        else:
            result = CONNECTION_ERROR_MSG
    except:
        pass
    else:
        result = response
    return result


def get_value_from_url(url, key):
    p, query = urllib.splitquery(url)
    if query is None:
        query = p
    return get_value_from_query(query, key)


def get_value_from_query(query, key):
    value = ""
    for q in query.split("&"):
        k, val = urllib.splitvalue(q)
        if k == key:
            value = val
            break
    # val = ""
    # if url.find('code=') != -1:
    #     p, query = urllib.splitquery(url)
    #     for q in query.split('&'):
    #         if q.find('code=') == 0:
    #             k, val = urllib.splitvalue(q)
    return value


def update_access_token(code):
    global my_client_id
    global access_token
    access_token = ''
    token = ""
    try:
        if code:
            token = getThingiverseAuthToken(code)
    except Exception as e:
        msg = e.message
        try:
            js = json.loads(msg)
        except:
            pass
        else:
            if isinstance(js, dict):
                msg = js.get('error')
        return msg
    else:
        if token:
            access_token = token
            save_token()
    return bool(access_token)


def logout_logic():
    global access_token
    access_token = ''
    save_token()


def dataToCollections(page):
    c = None
    try:
        js = json.loads(page)
    except:
        pass
    else:
        c = Collections.create_from_list(js)
    return c


def dataToThings(page):
    t = None
    try:
        js = json.loads(page)
    except:
        pass
    else:
        t = Things.create_from_list(js)
    return t


def dataToThingFiles(page):
    tf = None
    try:
        js = json.loads(page)
    except:
        pass
    else:
        tf = ThingFiles.create_from_list(js)
    return tf


def get_collections(username):
    global access_token
    col = None
    if access_token:
        url = "%s/users/%s/collections" % (THINGIVERSE_API_URL, username)
        # url = "%s/collections" % (THINGIVERSE_API_URL)
        req = createGetRequest(url, {'access_token': access_token})
        res = sendRequest(req)
        if not isResponse(res):
            raise Exception(res)
        else:
            data = res.read()
            col = dataToCollections(data)
    return col


def get_collection_things(c_id):
    global access_token
    thing = None
    if access_token:
        url = "%s/collections/%s/things" % (THINGIVERSE_API_URL, c_id)
        req = createGetRequest(url, {'access_token': access_token})
        res = sendRequest(req)
        if not isResponse(res):
            raise Exception(res)
        else:
            data = res.read()
            thing = dataToThings(data)
    return thing


def get_thing_files(t_id):
    global access_token
    t_files = None
    if access_token:
        url = "%s/things/%s/files" % (THINGIVERSE_API_URL, t_id)
        req = createGetRequest(url, {'access_token': access_token})
        res = sendRequest(req)
        if not isResponse(res):
            raise Exception(res)
        else:
            data = res.read()
            t_files = dataToThingFiles(data)
    return t_files


#####################################################################################
# Thingiverse data parts
#####################################################################################
class Creator(object):
    def __init__(self, cr_id, name, first_name, last_name, url, public_url, thumbnail):
        object.__init__(self)
        self.id = cr_id
        self.name = name
        self.first_name = first_name
        self.last_name = last_name
        self.url = url
        self.public_url = public_url
        self.thumbnail = thumbnail

    @staticmethod
    def create_from_dict(dct):
        if not isinstance(dct, dict):
            return None
        cr_id = dct.get('id')
        name = dct.get('name')
        first_name = dct.get('first_name')
        last_name = dct.get('last_name')
        url = dct.get('url')
        public_url = dct.get('public_url')
        thumbnail = dct.get('thumbnail')
        return Creator(cr_id, name, first_name, last_name, url, public_url, thumbnail)


class Collection(object):
    def __init__(self, c_id, name, url, count, creator, description, added, modified, is_editable,
                 thumbnail, thumbnail_1, thumbnail_2, thumbnail_3):
        object.__init__(self)
        self.id = c_id
        self.name = name
        self.url = url
        self.count = count
        self.creator = creator
        self.description = description
        self.added = added
        self.modified = modified
        self.is_editable = is_editable
        self.thumbnail = thumbnail
        self.thumbnail_1 = thumbnail_1
        self.thumbnail_2 = thumbnail_2
        self.thumbnail_3 = thumbnail_3
        self.things = None

    @staticmethod
    def create_from_dict(dct):
        if not isinstance(dct, dict):
            return None
        c_id = dct.get('id')
        name = dct.get('name')
        url = dct.get('url')
        count = dct.get('count')
        description = dct.get('description')
        added = dct.get('added')
        modified = dct.get('modified')
        is_editable = dct.get('is_editable')
        cr = dct.get('creator')
        thumbnail = dct.get('thumbnail')
        thumbnail_1 = dct.get('thumbnail_1')
        thumbnail_2 = dct.get('thumbnail_2')
        thumbnail_3 = dct.get('thumbnail_3')
        creator = Creator.create_from_dict(cr)
        return Collection(c_id, name, url, count, creator, description, added, modified, is_editable,
                 thumbnail, thumbnail_1, thumbnail_2, thumbnail_3)


class Collections(list):
    @staticmethod
    def create_from_list(lst):
        c = Collections()
        for item in lst:
            cfd = Collection.create_from_dict(item)
            if cfd is not None:
                c.append(cfd)
        return c

    def get_collection_by_id(self, c_id):
        co = None
        for c in self:
            if c_id == c.id:
                co = c
                break
        return co

    def get_collections_names(self):
        return [c.name for c in self]

    def get_collections_ids(self):
        return [c.id for c in self]

    def __str__(self):
        return "Collections object with %s items" % self.__len__()

    def __unicode__(self):
        return u"Collections object with %s items" % self.__len__()


class Thing(object):
    def __init__(self, t_id, name, url, public_url, creator, is_purchased, is_published, is_private, thumbnail):
        object.__init__(self)
        self.id = t_id
        self.name = name
        self.url = url
        self.public_url = public_url
        self.creator = creator
        self.is_purchased = is_purchased
        self.is_published = is_published
        self.is_private = is_private
        self.thumbnail = thumbnail
        self.thing_files = None

    @staticmethod
    def create_from_dict(dct):
        if not isinstance(dct, dict):
            return None
        t_id = dct.get('id')
        name = dct.get('name')
        url = dct.get('url')
        public_url = dct.get('public_url')
        cr = dct.get('creator')
        thumbnail = dct.get('thumbnail')
        is_purchased = dct.get('is_purchased')
        is_published = dct.get('is_published')
        is_private = dct.get('is_private')
        creator = Creator.create_from_dict(cr)
        return Thing(t_id, name, url, public_url, creator, is_purchased, is_published, is_private, thumbnail)

    # def download_thing_files(self, force=False):  # Experimental
    #     if (self.thing_files is not None) and (not force):
    #         print("Thing files exists")
    #         return True
    #     t_id = self.id
    #     self.thing_files = get_thing_files(t_id)
    #     return bool(self.thing_files)


class Things(list):
    @staticmethod
    def create_from_list(lst):
        t = Things()
        for item in lst:
            tfd = Thing.create_from_dict(item)
            if tfd is not None:
                t.append(tfd)
        return t

    def get_thing_by_id(self, t_id):
        th = None
        for t in self:
            if t_id == t.id:
                th = t
                break
        return th

    def get_things_names(self):
        return [t.name for t in self]

    def get_things_ids(self):
        return [t.id for t in self]

    def __str__(self):
        return "Things object with %s items" % self.__len__()

    def __unicode__(self):
        return u"Things object with %s items" % self.__len__()

    # def download_thing_files(self, force=False):  # Experimental
    #     res = True
    #     for t in self:
    #         try:
    #             if not t.download_thing_files(force):
    #                 print('Files not downloaded for Thing {}'.format(t.name))
    #                 res = False
    #         except Exception as e:
    #             mes = "While downloading file {} error occured: {}".format(t.name, e.message)
    #             raise Exception(mes)
    #     return res


class ThingFile(object):
    def __init__(self, tf_id, name, url, public_url, download_url, size, date, thumbnail):
        self.id = tf_id
        self.name = name
        self.url = url
        self.public_url = public_url
        self.download_url = download_url
        self.size = size
        self.date = date
        self.thumbnail = thumbnail

    @staticmethod
    def create_from_dict(dct):
        tf_id = dct.get('id')
        name = dct.get('name')
        url = dct.get('url')
        public_url = dct.get('public_url')
        download_url = dct.get('download_url')
        size = dct.get('size')
        date = dct.get('date')
        thumbnail = dct.get('thumbnail')
        return ThingFile(tf_id, name, url, public_url, download_url, size, date, thumbnail)


class ThingFiles(list):
    @staticmethod
    def create_from_list(lst):
        tf = ThingFiles()
        for item in lst:
            tffd = ThingFile.create_from_dict(item)
            if tffd is not None:
                tf.append(tffd)
        return tf

    def get_file_names(self):
        return [c.name for c in self]

    def get_file_ids(self):
        return [c.id for c in self]

    def get_file_download_urls(self):
        return [c.download_url for c in self]

    def __str__(self):
        return "ThingFiles object with %s items" % self.__len__()

    def __unicode__(self):
        return u"ThingFiles object with %s items" % self.__len__()

